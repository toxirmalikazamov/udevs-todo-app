import React from "react";
import "./todoList.css";
import TodoListItem from "../todoListItem/todoListItem";
import { ListGroup } from "react-bootstrap";

export default function TodoList({
  todos,
  onToggleDone,
  onToggleImportant,
  onDelete,
}) {
  return (
    <ListGroup as="ul">
      {todos.map((item, index) => (
        <ListGroup.Item key={index} className="todo-list" as="li">
          <TodoListItem
            {...item}
            onToggleDone={onToggleDone}
            onToggleImportant={onToggleImportant}
            onDelete={onDelete}
          />
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
}
