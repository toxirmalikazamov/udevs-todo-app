import React from "react";
import "./filterPanel.css";
import { ButtonGroup, Button } from "react-bootstrap";

export default function FiterPanel() {
  return (
    <ButtonGroup className="d-inline-block filter-panel pl-4">
      <Button variant="info">All</Button>
      <Button variant="outline-success">Active</Button>
      <Button variant="outline-success">Done</Button>
    </ButtonGroup>
  );
}
