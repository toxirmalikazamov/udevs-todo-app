import React from "react";
import "./appHeader.css";

export default function AppHeader() {
  return (
    <div className="app-header">
      <h1>Todo List</h1>
      <h5 className="text-muted">3 more to do, 0 done</h5>
    </div>
  );
}
