import { useState } from "react";
import "./App.css";
import AppHeader from "./components/appHeader/appHeader";
import TodoList from "./components/todoList/todoList";
import FilterPanel from "./components/fiterPanel/fiterPanel";
import SearchPanel from "./components/searchPanel/searchPanel";
import TodoAddForm from "./components/todoAddForm/todoAddForm";
import { Container, Row, Col } from "react-bootstrap";

let maxId = 100;

function createTodoItem(label) {
  return {
    label,
    important: false,
    done: false,
    id: maxId++,
  };
}

const initialTodos = [
  createTodoItem("Drink Coffee"),
  createTodoItem("Make Awesome App"),
  createTodoItem("Hava a lunch"),
];

function App() {
  const [todos, setTodos] = useState(initialTodos);

  function addTodo(label) {
    const newTodo = createTodoItem(label);
    setTodos([...todos, newTodo]);
  }

  function onToggleDone(todoId) {
    const toggleTodo = todos.find((item) => item.id === todoId);
    toggleTodo.done = !toggleTodo.done;
    const newTodo = todos.filter((item) => item.id !== todoId);
    setTodos([...newTodo, toggleTodo]);
  }

  function onToggleImportant(todoId) {
    const toggleTodo = todos.find((item) => item.id === todoId);
    toggleTodo.important = !toggleTodo.important;
    const newTodo = todos.filter((item) => item.id !== todoId);
    setTodos([...newTodo, toggleTodo]);
  }

  function onDelete(todoId) {
    const newTodo = todos.filter((item) => item.id !== todoId);
    setTodos([...newTodo]);
  }

  return (
    <div className="App">
      <Container>
        <Row className="my-5">
          <Col md={{ span: 4, offset: 4 }}>
            <AppHeader />
            <SearchPanel />
            <FilterPanel />
            <TodoList
              todos={todos}
              onToggleDone={onToggleDone}
              onToggleImportant={onToggleImportant}
              onDelete={onDelete}
            />
            <TodoAddForm addTodo={addTodo} />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
